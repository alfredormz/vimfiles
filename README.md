#Usage

```bash
sudo apt-get install vim vim-gtk
git clone https://bitbucket.org/alfredormz/vimfiles.git ~/.vim
cd ~/.vim
git submodule update --init
echo "source ~/.vim/vimrc" > ~/.vimrc
vim +PluginInstall +qall
```

#Plugins

* gmarik/vundle
* scrooloose/nerdcommenter
* scrooloose/nerdtree
* altercation/vim-colors-solarized
* airblade/vim-gitgutter
* tpope/vim-fugitive
* scrooloose/syntastic
* chrisbra/csv.vim
* bling/vim-airline
* kien/ctrlp.vim
